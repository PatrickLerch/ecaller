package com.hali.eCaller.authserver.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.hali.eCaller.authserver.model.User;

public interface UserRepository extends MongoRepository<User, Long> {
    User findByEmail(String email);
}
