package com.hali.eCaller.authserver.model;

public enum AuthorityName {
    ROLE_USER, ROLE_ADMIN
}
